﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_2
{
    public partial class Form1 : Form
    {
        String currentFont = "Agency FB";  //Storing current font
        int currentSize = 20;  //Storing current size
        int currentFormat = 0;  //Storing current format
        public Form1()
        {
            InitializeComponent();
        }

        private void fontChoose_SelectedIndexChanged(object sender, EventArgs e)  //Called when list box option changed
        {
            String currentItem = fontChoose.SelectedIndex.ToString();  //Storing current selection into string
            switch (currentItem)  //Using switch statement to find which has been selected
            {
                case "0":  //Selection 1 or Agency FB
                    currentFont = "Agency FB"; //Setting font to Agency FB
                    break;
                case "1":  //Selection 2 or Mistral
                    currentFont = "Mistral"; //Setting font to Mistral
                    break;
                case "2":  //Selection 3 or Corbel
                    currentFont = "Corbel"; //Setting font to Corbel
                    break;
                default:  //In case of error, default to Agency FB
                    currentFont = "Agency FB";
                    break;
            }
            updateText();  //Updating text
        }

        private void big_CheckedChanged(object sender, EventArgs e)  //When radio button for big text clicked
        {
            currentSize = 20;  //Setting size to 20
            updateText();  //Updating text
        }

        private void small_CheckedChanged(object sender, EventArgs e)  //When radio button for small text clicked
        {
            currentSize = 13;  //Setting size to 13
            updateText();  //Updating text
        }

        private void bold_CheckedChanged(object sender, EventArgs e)  //When bold box clicked
        {
            switch (currentFormat)
            {
                //If had no bold
                case 0:
                    currentFormat = 2;
                    break;
                //If had no bold, but had italics
                case 1:
                    currentFormat = 3;
                    break;
                //If it had bold
                case 2:
                    currentFormat = 0;
                    break;
                //If it had bold and italics
                case 3:
                    currentFormat = 1;
                    break;
                //In case of error
                default:
                    currentFormat = 0;
                    break;
            }
            updateText();  //Updating text
        }
        private void italics_CheckedChanged(object sender, EventArgs e)  //When italics box clicked
        {
            switch (currentFormat)
            {
                //If had no italics
                case 0:
                    currentFormat = 1;
                    break;
                //If had italics
                case 1:
                    currentFormat = 0;
                    break;
                //If had no italics, but had bold
                case 2:
                    currentFormat = 3;
                    break;
                //If had italics and bold
                case 3:
                    currentFormat = 2;
                    break;
                //In case of error
                default:
                    currentFormat = 0;
                    break;
            }
            updateText();  //Updating text
        }

        private void updateText()
        {
            switch (currentFormat)  //Finding current format and updating
            {
                //Has normal format
                case 0:
                    text.Font = new Font(currentFont, currentSize, FontStyle.Regular);
                    break;
                //Has italics
                case 1:
                    text.Font = new Font(currentFont, currentSize, FontStyle.Italic);
                    break;
                //Has bold
                case 2:
                    text.Font = new Font(currentFont, currentSize, FontStyle.Bold);
                    break;
                //Has bold and italics
                case 3:
                    text.Font = new Font(currentFont, currentSize, FontStyle.Italic | FontStyle.Bold);
                    break;
                //In case of error
                default:
                    text.Font = new Font(currentFont, currentSize, FontStyle.Regular);
                    break;
            }
        }

        private void fillOptions_Enter(object sender, EventArgs e)
        {
        }



        private void text_Click(object sender, EventArgs e)
        {

        }
    }
}
