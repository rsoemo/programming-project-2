﻿namespace Programming_Project_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontChoose = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.big = new System.Windows.Forms.RadioButton();
            this.small = new System.Windows.Forms.RadioButton();
            this.sizeOptions = new System.Windows.Forms.GroupBox();
            this.bold = new System.Windows.Forms.CheckBox();
            this.italics = new System.Windows.Forms.CheckBox();
            this.showOptions = new System.Windows.Forms.GroupBox();
            this.text = new System.Windows.Forms.Label();
            this.sizeOptions.SuspendLayout();
            this.showOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // fontChoose
            // 
            this.fontChoose.FormattingEnabled = true;
            this.fontChoose.Items.AddRange(new object[] {
            "Agency FB",
            "Mistral",
            "Corbel"});
            this.fontChoose.Location = new System.Drawing.Point(8, 26);
            this.fontChoose.Name = "fontChoose";
            this.fontChoose.Size = new System.Drawing.Size(131, 43);
            this.fontChoose.TabIndex = 0;
            this.fontChoose.SelectedIndexChanged += new System.EventHandler(this.fontChoose_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select a Font";
            // 
            // big
            // 
            this.big.AutoSize = true;
            this.big.Location = new System.Drawing.Point(3, 19);
            this.big.Name = "big";
            this.big.Size = new System.Drawing.Size(52, 17);
            this.big.TabIndex = 2;
            this.big.TabStop = true;
            this.big.Text = "Large";
            this.big.UseVisualStyleBackColor = true;
            this.big.CheckedChanged += new System.EventHandler(this.big_CheckedChanged);
            // 
            // small
            // 
            this.small.AutoSize = true;
            this.small.Location = new System.Drawing.Point(3, 42);
            this.small.Name = "small";
            this.small.Size = new System.Drawing.Size(50, 17);
            this.small.TabIndex = 3;
            this.small.TabStop = true;
            this.small.Text = "Small";
            this.small.UseVisualStyleBackColor = true;
            this.small.CheckedChanged += new System.EventHandler(this.small_CheckedChanged);
            // 
            // sizeOptions
            // 
            this.sizeOptions.Controls.Add(this.small);
            this.sizeOptions.Controls.Add(this.big);
            this.sizeOptions.Location = new System.Drawing.Point(12, 80);
            this.sizeOptions.Name = "sizeOptions";
            this.sizeOptions.Size = new System.Drawing.Size(128, 73);
            this.sizeOptions.TabIndex = 4;
            this.sizeOptions.TabStop = false;
            this.sizeOptions.Text = "Select Size Options";
            this.sizeOptions.Enter += new System.EventHandler(this.fillOptions_Enter);
            // 
            // bold
            // 
            this.bold.AutoSize = true;
            this.bold.Location = new System.Drawing.Point(3, 43);
            this.bold.Name = "bold";
            this.bold.Size = new System.Drawing.Size(47, 17);
            this.bold.TabIndex = 5;
            this.bold.Text = "Bold";
            this.bold.UseVisualStyleBackColor = true;
            this.bold.CheckedChanged += new System.EventHandler(this.bold_CheckedChanged);
            // 
            // italics
            // 
            this.italics.AutoSize = true;
            this.italics.Location = new System.Drawing.Point(3, 20);
            this.italics.Name = "italics";
            this.italics.Size = new System.Drawing.Size(53, 17);
            this.italics.TabIndex = 6;
            this.italics.Text = "Italics";
            this.italics.UseVisualStyleBackColor = true;
            this.italics.CheckedChanged += new System.EventHandler(this.italics_CheckedChanged);
            // 
            // showOptions
            // 
            this.showOptions.Controls.Add(this.italics);
            this.showOptions.Controls.Add(this.bold);
            this.showOptions.Location = new System.Drawing.Point(12, 159);
            this.showOptions.Name = "showOptions";
            this.showOptions.Size = new System.Drawing.Size(128, 66);
            this.showOptions.TabIndex = 7;
            this.showOptions.TabStop = false;
            this.showOptions.Text = "Select Format Options";
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Font = new System.Drawing.Font("Corbel", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text.Location = new System.Drawing.Point(190, 115);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(108, 24);
            this.text.TabIndex = 8;
            this.text.Text = "Hello World";
            this.text.Click += new System.EventHandler(this.text_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 235);
            this.Controls.Add(this.text);
            this.Controls.Add(this.showOptions);
            this.Controls.Add(this.sizeOptions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fontChoose);
            this.Name = "Form1";
            this.Text = "Text Changer";
            this.sizeOptions.ResumeLayout(false);
            this.sizeOptions.PerformLayout();
            this.showOptions.ResumeLayout(false);
            this.showOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox fontChoose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton big;
        private System.Windows.Forms.RadioButton small;
        private System.Windows.Forms.GroupBox sizeOptions;
        private System.Windows.Forms.CheckBox bold;
        private System.Windows.Forms.CheckBox italics;
        private System.Windows.Forms.GroupBox showOptions;
        private System.Windows.Forms.Label text;
    }
}

